extends Node

export var Rock: PackedScene
export var Enemy: PackedScene

var screensize: = Vector2()
var level: = 0
var score: = 0
var playing: = false


# also connected by code in _on_EnemyTimer_timeout()
func _on_Player_shoot(bullet: PackedScene, pos: Vector2, dir: float) -> void:
	var b: = bullet.instance()
	b.start(pos, dir)
	add_child(b)


# connected by code in spawn_rock()
func _on_Rock_exploded(size: int, radius: int, pos: Vector2, vel: Vector2) -> void:
	$ExplodeSound.play()
	score += size*25
	$HUD.update_score(score)
	if size <= 1:
		return
	# run code with two value:-1 and 1
	for offset in [-1, 1]:
		# make a tangent direction. perpendicular to the axe rock-player
		var dir: = (pos - $Player.position).normalized().tangent() as Vector2
		# using the offset (-1 or 1) we have 2 opposite directions
		dir *= offset
		# newpos is after the current rock radius
		var newpos: = pos + dir*radius
		# velocity is 10% more
		var newvel: = dir*vel.length()*1.1

		# the folowing create a warning "Can't change this state while flushing queries"
		# spawn_rock( size - 1, newpos, newvel)
		call_deferred("spawn_rock", size - 1, newpos, newvel)


func _on_EnemyTimer_timeout() -> void:
	var e = Enemy.instance()
	add_child(e)
	e.target = $Player
	e.connect('shoot', self, '_on_Player_shoot')
	$EnemyTimer.wait_time = rand_range(20, 40)
	$EnemyTimer.start()


func _ready() -> void:
	randomize()
	screensize = get_viewport().get_visible_rect().size
	#$Player.screensize = screensize
	for _i in range(3):
		spawn_rock(3)


func _process(_delta: float) -> void:
	if playing and $Rocks.get_child_count() == 0:
		new_level()


func _input(event: InputEvent) -> void:
	if event.is_action_pressed('pause'):
		if not playing:
			return
		get_tree().paused = not get_tree().paused
		if get_tree().paused:
			$HUD/MessageLabel.text = "Paused"
			$HUD/MessageLabel.show()
		else:
			$HUD/MessageLabel.text = ""
			$HUD/MessageLabel.hide()


func spawn_rock(size: int, pos: = Vector2(), vel: = Vector2()) -> void:
	if !pos:
		# choose a random offset on the path between 0 (start) and 1 (end of the path)
		$RockPath/RockSpawn.set_offset(randi())
		# get the (interpolated) position associated to the given offset
		pos = $RockPath/RockSpawn.position
	if !vel:
		vel = Vector2(1, 0).rotated(rand_range(0, 2*PI))*rand_range(100, 150)

	var r = Rock.instance()
	r.screensize = screensize
	r.start(pos, vel, size)
	$Rocks.add_child(r)
	r.connect('exploded', self, '_on_Rock_exploded')


func new_game() -> void:
	$Music.play()
	for rock in $Rocks.get_children():
		rock.queue_free()
	level = 0
	score = 0
	$HUD.update_score(score)
	$Player.start()
	$HUD.show_message("Get Ready!")
	yield ($HUD/MessageTimer, "timeout")
	playing = true
	new_level()


func new_level() -> void:
	$LevelupSound.play()
	level += 1
	$HUD.show_message("Wave %s"%level)
	for _i in range(level):
		spawn_rock(3)
	$EnemyTimer.wait_time = rand_range(5, 10)
	$EnemyTimer.start()


func game_over() -> void:
	$Music.stop()
	playing = false
	$HUD.game_over()
