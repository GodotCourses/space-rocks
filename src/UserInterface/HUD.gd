extends CanvasLayer

signal start_game

onready var lives_counter: = [
	$MarginContainer/HBoxContainer/LivesCounter/L1,
	$MarginContainer/HBoxContainer/LivesCounter/L2,
	$MarginContainer/HBoxContainer/LivesCounter/L3
	]
onready var ShieldBar: = $MarginContainer/HBoxContainer/ShieldBar

var red_bar: = preload("res://assets/graphics/user_interface/barHorizontal_red_mid 200.png")
var yellow_bar: = preload("res://assets/graphics/user_interface/barHorizontal_yellow_mid 200.png")
var green_bar: = preload("res://assets/graphics/user_interface/barHorizontal_green_mid 200.png")


func _ready() -> void:
	$MessageLabel.hide()


func _on_StartButton_pressed() -> void:
	$StartButton.hide()
	emit_signal("start_game")


func _on_MessageTimer_timeout():
	$MessageLabel.hide()
	$MessageLabel.text = ''


func show_message(message: String) -> void:
	$MessageLabel.text = message
	$MessageLabel.show()
	$MessageTimer.start()


func update_score(value: int) -> void:
	$MarginContainer/HBoxContainer/ScoreLabel.text = str(value)


func update_lives(value: int) -> void:
	for item in range(3):
		lives_counter[item].visible = value > item


func update_shield(value) -> void:
	print(" update_shield %s" % value)
	ShieldBar.texture_progress = green_bar
	if value < 0.4:
		ShieldBar.texture_progress = red_bar
	elif value < 0.7:
		ShieldBar.texture_progress = yellow_bar
	ShieldBar.value = value


func game_over() -> void:
	show_message("Game Over")
	yield ($MessageTimer, "timeout")
	$StartButton.show()
