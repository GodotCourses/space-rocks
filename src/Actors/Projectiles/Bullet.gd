extends Area2D


export var speed: int
var velocity = Vector2()


func _on_Bullet_body_entered(body: Node) -> void:
	if body.is_in_group('rocks'):
		body.explode()
		queue_free()


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()


# the enemy is an Area2D, so it will not trigger _on_Bullet_body_entered signal.
# To detect the enemy, you need to also connect the  area_entered signal:
func _on_Bullet_area_entered(area: Area2D) -> void:
	if area.is_in_group('enemies'):
		area.take_damage(1)
	queue_free()



func _process(delta: float) -> void:
	position += velocity*delta


func start(pos: Vector2, angle: float) -> void:
	position = pos
	rotation = angle
	velocity = Vector2(speed, 0).rotated(angle)
