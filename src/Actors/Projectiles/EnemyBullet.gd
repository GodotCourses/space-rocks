extends Area2D

export var speed: int = 0

var velocity = Vector2()


func _process(delta: float) -> void:
	position += velocity*delta


func _on_EnemyBullet_body_entered(body: Node2D) -> void:
	if body.name == 'Player':
		body.shield -= 15
	queue_free()


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()


func start(_position: Vector2, _direction: float) -> void:
	position = _position
	velocity = Vector2(speed, 0).rotated(_direction)
	rotation = _direction
