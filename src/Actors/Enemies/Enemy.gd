extends Area2D

signal shoot

export var Bullet: PackedScene
export var speed: = 150
export var health: = 3

var target: RigidBody2D
var follow: PathFollow2D


func _on_AnimationPlayer_animation_finished(_anim_name: String) -> void:
	queue_free()


func _on_GunTimer_timeout() -> void:
	shoot_pulse(3, 0.15)


func _on_Enemy_body_entered(body: Node) -> void:
	if body.name == 'Player':
		body.shield -= 50
	explode()


func _ready() -> void:
	$Sprite.frame = randi()%3
	var path: Path2D = $EnemyPaths.get_children()[randi()%$EnemyPaths.get_child_count()]
	follow = PathFollow2D.new()
	path.add_child(follow)
	follow.loop = false


func _process(delta: float) -> void:
	follow.offset += speed*delta
	position = follow.global_position

	# detect the end of the path
	if follow.unit_offset >= 1:
		queue_free()


func shoot() -> void:
	$ShootSound.play()
	# find the vector pointing to the player's position
	var pos: = (target.global_position - global_position) as Vector2
	# add a little bit of randomness to it so that the bullets don't follow exactly the same path.
	var dir: = pos.rotated(rand_range(-0.1, 0.1)).angle()
	emit_signal('shoot', Bullet, global_position, dir)


func shoot_pulse(number: int, delay: float) -> void:
	for _i in range(number):
		shoot()
		yield (get_tree().create_timer(delay), 'timeout')


func take_damage(amount:int)-> void:
	health -= amount
	$AnimationPlayer.play('flash')
	if health <= 0:
		explode()
	yield ($AnimationPlayer, 'animation_finished')
	$AnimationPlayer.play('rotate')


func explode()-> void:
	$ExplosionSound.play()
	speed = 0
	$GunTimer.stop()
	$CollisionShape2D.disabled = true
	$Sprite.hide()
	$Explosion.show()
	$Explosion/AnimationPlayer.play("explosion")

