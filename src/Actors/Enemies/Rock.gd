extends RigidBody2D

signal exploded

var screensize: = Vector2()
var size: = 0
var radius: = 0
var scale_factor: = 0.2


func _integrate_forces(physics_state: Physics2DDirectBodyState) -> void:
		# changing _physics_process() to _integrate_forces() allows altering the transform's origin of a rigidopdy2D
	var xform: = physics_state.get_transform()
	if xform.origin.x > screensize.x + radius:
		xform.origin.x = 0 - radius
	if xform.origin.x < 0 - radius:
		xform.origin.x = screensize.x + radius
	if xform.origin.y > screensize.y + radius:
		xform.origin.y = 0 - radius
	if xform.origin.y < 0 - radius:
		xform.origin.y = screensize.y + radius
	physics_state.set_transform(xform)


func start(pos: Vector2, vel: Vector2, _size: int) -> void:
	position = pos
	size = _size
	mass = 1.5*size
	$Sprite.scale = Vector2(1, 1)*scale_factor*size
	radius = int($Sprite.texture.get_size().x/2*scale_factor*size)
	# we create a NEW shape to make it unique and not shared between all instanciated rocks
	# var shape:=$CollisionShape2D.shape as CircleShape2D # this is a shared shape
	var shape: = CircleShape2D.new()
	shape.radius = radius
	$CollisionShape2D.shape = shape
	linear_velocity = vel
	angular_velocity = rand_range(-2, 2)
	$Explosion.scale = Vector2(0.75, 0.75)*size


func explode() -> void:
	layers = 0
	$Sprite.hide()
	$Explosion/AnimationPlayer.play("explosion")
	emit_signal("exploded", size, radius, position, linear_velocity)
	linear_velocity = Vector2()
	angular_velocity = 0


func _on_AnimationPlayer_animation_finished(_anim_name: String) -> void:
	queue_free()
