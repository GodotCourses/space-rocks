extends RigidBody2D

enum States { INIT, ALIVE, INVULNERABLE, DEAD }

signal shoot
signal lives_changed
signal shield_changed

export var max_shield: = 100
export var shield_regen: = 2.0
export var engine_power: = 0
export var spin_power: = 0
export var fire_rate: = 0.0
export var Bullet: PackedScene

var state: = 0
var thrust: = Vector2()
var rotation_dir: = 0
var screensize: = Vector2()
var can_shoot: = true
var lives: = 0 setget set_lives
var shield: = 100.0 setget set_shield


func _on_Player_body_entered(body: Node) -> void:
	# To allow contact detection (and _on_Player_body_entered signal)
	# Select the Player node and in the Inspector, set Contact Monitoring to On
	# By default, no contacts are reported, so set Contacts Reported to 1
	if body.is_in_group('rocks'):
		body.explode()
		self.shield -= body.size*25


func _on_GunTimer_timeout() -> void:
	can_shoot = true


func _on_InvulnerabilityTimer_timeout() -> void:
	change_state(States.ALIVE)


func _on_AnimationPlayer_animation_finished(_anim_name: String) -> void:
	$Explosion.hide()


func _ready() -> void:
	change_state(States.INIT)
	screensize = get_viewport().get_visible_rect().size
	$GunTimer.wait_time = fire_rate


func _process(delta: float) -> void:
	self.shield += shield_regen*delta
	get_input()


func _integrate_forces(physics_state: Physics2DDirectBodyState) -> void:
	# changing _physics_process() to _integrate_forces() allows altering the transform's origin of a rigidopdy2D
	set_applied_force(thrust.rotated(rotation))
	set_applied_torque(spin_power*rotation_dir)

	# wrap player position
	var xform: = physics_state.get_transform()
	if xform.origin.x > screensize.x:
		xform.origin.x = 0
	if xform.origin.x < 0:
		xform.origin.x = screensize.x
	if xform.origin.y > screensize.y:
		xform.origin.y = 0
	if xform.origin.y < 0:
		xform.origin.y = screensize.y
	physics_state.set_transform(xform)


func set_lives(value: int) -> void:
	lives = value
	emit_signal("lives_changed", lives)
	self.shield = max_shield
	if lives <= 0:
		call_deferred("change_state", States.DEAD)
	else:
		call_deferred("change_state", States.INVULNERABLE)


func set_shield(value: float) -> void:
	if value > max_shield:
		value = max_shield
	shield = value
	emit_signal("shield_changed", shield/max_shield)
	if shield <= 0:
		explode()
		self.lives -= 1


func get_input() -> void:
	thrust = Vector2()
	rotation_dir = 0
	$Exhaust.emitting = false
	if state in [States.DEAD, States.INIT]:
		return

	if Input.is_action_pressed("thrust"):
		thrust = Vector2(engine_power, 0)
		$Exhaust.emitting = true
		if !$EngineSound.playing:
			$EngineSound.play()
	else:
		$EngineSound.stop()
	if Input.is_action_pressed("rotate_right"):
		rotation_dir += 1
	if Input.is_action_pressed("rotate_left"):
		rotation_dir -= 1
	if can_shoot and Input.is_action_pressed("shoot"):
		shoot()


func change_state(new_state: int) -> void:
	match new_state:
		States.INIT:
			$CollisionShape2D.disabled = true
			$Sprite.modulate.a = 0.5
		States.ALIVE:
			$Sprite.show()
			$CollisionShape2D.disabled = false
			$Sprite.modulate.a = 1.0
		States.INVULNERABLE:
			$Sprite.show()
			$CollisionShape2D.disabled = true
			$Sprite.modulate.a = 0.5
			$InvulnerabilityTimer.start()
		States.DEAD:
			$EngineSound.stop()
			$CollisionShape2D.disabled = true
			$Sprite.hide()
			linear_velocity = Vector2()
	state = new_state


func start() -> void:
	$Sprite.show()
	self.lives = 3
	change_state(States.ALIVE)
	self.shield = max_shield


func shoot() -> void:
	if state == States.INVULNERABLE:
		return
	emit_signal("shoot", Bullet, $Muzzle.global_position, rotation)
	can_shoot = false
	$LaserSound.play()
	$GunTimer.start()


func explode() -> void:
	$ExplosionSound.play()
	$Sprite.hide()
	$Explosion.show()
	$Explosion/AnimationPlayer.play("explosion")
