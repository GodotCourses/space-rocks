## Space Rocks

### Description

Jeu de type "Asteroid Like", créé dans le cadre du livre "Godot Engine Game Development Projects".

### Objectifs

Tirez sur les rochers et les ennemis et évitez d'être touché par des débris et/ou des tirs ennemis.

### Mouvements et actions du joueur

Rotation à gauche: flèche gauche, touche Q, touche D-Pad gauche du Joystick
Rotation à droite: flèche droite, touche D, touche D-Pad droite du Joystick
Accélération: flèche haut, touche Z, touche D-Pad Haut du Joystick
Tir: touche espace, touche R2 du Joystick
Pause: touche P ou touche Y du Joystick
Quitter le jeu: clic sur la croix.

### Interactions

Le joueur peut tirer.
Les ennemis peuvent tirer.
Les astéroïdes peuvent toucher et endommager les ennemis et le joueur.

### Copyrights

Codé en GDscript en utilisant le moteur de jeu Godot Engine.
Développé en utilisant principalement l'éditeur de code intégré et/ou Visual Studio Code.

-----------------
(C) 2020 GameaMea (http://www.gameamea.com)

=================================================

### Description

"Asteroid Like" type game, created as part of the book "Godot Engine Game Development Projects".

### Goals

Shoot rocks and enemies and avoid been touched by debris and/or enemy fires.

### Movements and actions of the player

Left rotation: left arrow, Q key, left D-Pad key on Joystick
Right rotation: right arrow, D key, right D-Pad key on Joystick
Acceleration: up arrow, Z key, D-Pad Up key on Joystick
Shoot: space key, R2 key on Joystick
Pause: P key ou Y key on Joystick
Exit the game: click on the cross.

### Interactions

The player can shoot.
The enemies can shoot.
The asteroids can touch and damage the enemies and the player.

### Copyrights

Coded in GDscript using the Godot Engine game engine.
Developed using mainly the integrated code editor and/or Visual Studio Code.

-----------------
(C) 2020 GameaMea (http://www.gameamea.com)
